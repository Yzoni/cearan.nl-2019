Toevoegen van afbeeldingen:

1. Voeg de afbeelding toe aan `assets/posters/`
2. Voeg de naam zonder de extensie toe aan het bestand `_data/posters.yml`

Toevoegen geluid:

1. Voeg het geluid toe aan `assets/sound`
2. Voeg de naam zonder de extensie toe aan het bestand `_data/sounds.yml`